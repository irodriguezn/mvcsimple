/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import modelo.Alumno;
import vista.VAlumno;

/**
 *
 * @author nacho
 */
public class Controlador {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // El controlador lleva la lógica del programa. Importa siempre las
        // clases del Modelo y de la Vista y a partir de ahí hace todo.
        // Normalmente al leer los datos se podrá producir algún error
        // de Entrada/Salida que estaremos obligados a tratar.
        
        VAlumno vista=new VAlumno();
        try {
            Alumno a1=vista.leerAlumno();
            vista.mostrarAlumno(a1);
        } catch (IOException e) {
            System.out.println("Error leyendo datos");
        }
    }
    
}
