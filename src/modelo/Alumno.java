/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author nacho
 */
public class Alumno {
    // En los archivos del modelo tenemos las clases que definen nuestro
    // modelo de datos. No haremos ninguna operación de Entrada/Salida, de eso
    // ya se encargará la vista. Así que nada de utilizar println o scanner
    
    String nombre;
    int edad;

    public Alumno(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
}
