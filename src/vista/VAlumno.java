/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import java.util.Scanner;
import modelo.Alumno;

/**
 *
 * @author nacho
 */
public class VAlumno {
    // La vista se encarga de hacer todas las operaciones de Entrada/Salida
    // de recoger y mostrar datos. Normalmente lanzará excepciones de E/S
    // que tendrá que recoger el controlador, aunque en este caso no haría falta
    
    public Alumno leerAlumno() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca el nombre: ");
        String nombre=sc.next();
        System.out.print("Introduzca la edad: ");
        int edad=sc.nextInt();
        return new Alumno(nombre, edad);
    }
    
    public void mostrarAlumno(Alumno alu) {
        System.out.println("Alumno{" + "nombre=" + alu.getNombre() + ", edad=" + alu.getEdad() + '}');
    }
}
